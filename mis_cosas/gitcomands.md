# Comandos de git (basicos)

```
git clone link (https://gitlab.com..)
```
Para clonar un repostorio publico y descargarlo para tu Pc

```
git status 
```
Es para revisar si hay un cambio en el repositorio o proyecto.

```
git pull
```
Para descargar repositorios

```
git push
```
Para subir el repositorio o actualizar los cambios.

```
git add
```
Para actualizar los nuevos cambios

```
git commit
```
Para dejar un comentario en la actualización

```
git checkout
```
Para cambiar de tag (etiqueta)

```
git checkout -b exercici4
```

```
git branch -d prova3
```

```
git push origin --delete prova3
```

```
git stash
```

```
git stash apply
```

```
git diff
```

```
git branch
```

```
git remote -v
```

```
git merge
```

